# plotting.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import rcParams
from mpl_toolkits.mplot3d import Axes3D

colors = [
        "#74CE74",
        "#FFA500",
        "#F7523A",
        "#5A5A5A",
        "#0759AB",
        "y",
        "m",
        "g"]
        
basecolors = {
    "real_front"    : "#7C8577",
    "z_ideal"       : "#ECF037",
    "z_nadir"       : "#F03785",
    "to_keep"       : "#8CDC6B",
    "to_replace"    : "#620F0E",
    "almost_black"  : "#262626",
    "weights"       : "#E4AFC8",
    "weights_hl"    : "#7D2950",
    "cherry_dark"   : "#A91458",
    "cherry_light"  : "#E4AFC8",
    "jitter"        : "#74CE74",
}
        
def load_rcparams(figsize=None, for_paper=False):
    """
    Load a custom rcParams dictionary
    """
    
    rcParams['axes.titlesize']  = 14            # title
    rcParams['axes.labelsize']  = 17            # $f_i$ labels
    rcParams['xtick.color']     = "#474747"     # ticks gray color
    rcParams['ytick.color']     = "#474747"     # ticks gray color
    rcParams['xtick.labelsize'] = 10            # ticks size
    rcParams['ytick.labelsize'] = 10            # ticks size
    rcParams['legend.fontsize'] = 12            # legend
    rcParams['legend.fontsize'] = 12            # legend

    if for_paper:    
        rcParams['font.family'] = 'serif'       # font face
                
    if isinstance(figsize, tuple):
        rcParams['figure.figsize'] = figsize    # figsize, common values: (12, 5), (8, 6)
    

def plot_pops(pops, labels, merged_pops, best_front, save=False):
    
    load_rcparams((27, 4.2))
    
    fig = plt.figure()
    axes = (fig.add_subplot(161, projection="3d"),
            fig.add_subplot(162, projection="3d"),
            fig.add_subplot(163, projection="3d"),
            fig.add_subplot(164, projection="3d"),
            fig.add_subplot(165, projection="3d"),
            fig.add_subplot(166, projection="3d"),
    )
    
    letters = ("(a)", "(b)", "(c)", "(d)", "(e)")
    
    for i, pop in enumerate(pops):
    
        ax = axes[i]
    
        ax.plot(pop[:, 0], pop[:, 1], pop[:, 2], label=labels[i], marker="o", ls="none", c=colors[0], 
            mec=basecolors["almost_black"], markeredgewidth=0.15)
    
    
        ax.set_title("%s %s" % (letters[i], labels[i]))
        #ax.legend(loc="upper right", ncol=1, scatterpoints=1, numpoints=1)
        
        ax.set_xlabel("$f_1$")
        ax.set_ylabel("$f_2$")
        ax.set_zlabel("$f_3$")
        
        ax.set_xlim(0, 1.05)
        ax.set_ylim(0, 1.05)
        ax.set_zlim(0, 1.05)
        
        #ax.set_xticklabels()
        ax.grid()
        ax.view_init(30, 45)
        
    # last ax
    ax = axes[-1]
    
    ax.plot(merged_pops[:, 0], merged_pops[:, 1], merged_pops[:, 2], label="merged_pops", marker="o", ls="none", c=colors[0], 
            mec=basecolors["almost_black"], markeredgewidth=0.15)
    
    ax.plot(merged_pops[best_front, 0], merged_pops[best_front, 1], merged_pops[best_front, 2], label="merged_pops[nds]", marker="o", ls="none", c=colors[1], 
            mec=basecolors["almost_black"], markeredgewidth=0.15)
            
    ax.set_xlabel("$f_1$")
    ax.set_ylabel("$f_2$")
    ax.set_zlabel("$f_3$")
    
    ax.set_xlim(0, 2.05)
    ax.set_ylim(0, 2.05)
    ax.set_zlim(0, 2.05)
    
    #ax.set_xticklabels()
    ax.grid()
    ax.view_init(30, 45)
    
    ax.legend(loc="upper right", scatterpoints=1, numpoints=1)
    
    # setup main title
    #fig.suptitle(mop_label, 
    #    fontsize=14, 
    #    horizontalalignment="center")
    
    # setup margins
    plt.subplots_adjust(
            bottom=0.12,
            wspace=0.1,
            left=0.04, 
            right=0.98,
            )
    
    if save:
        output = "fig_dci_pops.png"
        fig.savefig(output, dpi=300)
        print(output)
        
    plt.show()

def plot_irregular_pops(pops, labels, scales, save=False):
    
    load_rcparams((27, 4.2))
    
    fig = plt.figure()
    axes = (fig.add_subplot(151, projection="3d"),
            fig.add_subplot(152, projection="3d"),
            fig.add_subplot(153, projection="3d"),
            fig.add_subplot(154, projection="3d"),
            fig.add_subplot(155, projection="3d"),
    )
    
    letters = ("(a)", "(b)", "(c)", "(d)", "(e)")
    
    for i, pop in enumerate(pops):
    
        ax = axes[i]
    
        ax.plot(pop[:, 0], pop[:, 1], pop[:, 2], label=labels[i], marker="o", ls="none", c=colors[0], 
            mec=basecolors["almost_black"], markeredgewidth=0.15)
    
    
        ax.set_title("%s %s" % (letters[i], labels[i]))
        #ax.legend(loc="upper right", ncol=1, scatterpoints=1, numpoints=1)
        
        ax.set_xlabel("$f_1$")
        ax.set_ylabel("$f_2$")
        ax.set_zlabel("$f_3$")
        
        ax.set_xlim(0, scales[0])
        ax.set_ylim(0, scales[1])
        ax.set_zlim(0, scales[2])
        
        #ax.set_xticklabels()
        ax.grid()
        ax.view_init(30, 45)
        
    # last ax
    ax = axes[-1]
    ax.legend(loc="upper right", scatterpoints=1, numpoints=1)
    
    # setup main title
    #fig.suptitle(mop_label, 
    #    fontsize=14, 
    #    horizontalalignment="center")
    
    # setup margins
    plt.subplots_adjust(
            bottom=0.12,
            wspace=0.1,
            left=0.04, 
            right=0.98,
            )
    
    if save:
        output = "fig_dci_irregular_pops.png"
        fig.savefig(output, dpi=300)
        print(output)
        
    plt.show()


def plot_iso(merged_pops, best_front, save=False):
    
    load_rcparams((8, 8))
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    
    ax.plot(merged_pops[:, 0], merged_pops[:, 1], merged_pops[:, 2], label="merged_pops", marker="o", ls="none", c=colors[0], 
            mec=basecolors["almost_black"], markeredgewidth=0.15)
    
    ax.plot(merged_pops[best_front, 0], merged_pops[best_front, 1], merged_pops[best_front, 2], label="merged_pops[nds]", marker="o", ls="none", c=colors[1], 
            mec=basecolors["almost_black"], markeredgewidth=0.15)
            
    ax.set_xlabel("$f_1$")
    ax.set_ylabel("$f_2$")
    ax.set_zlabel("$f_3$")
    
    ax.set_xlim(0, 2.05)
    ax.set_ylim(0, 2.05)
    ax.set_zlim(0, 2.05)
    
    #ax.set_xticklabels()
    ax.grid()
    ax.view_init(30, 45)
    
    ax.legend(loc="lower right", scatterpoints=1, numpoints=1)
    
    # setup main title
    #fig.suptitle(mop_label, 
    #    fontsize=14, 
    #    horizontalalignment="center")
    
    # setup margins
    plt.subplots_adjust(
            bottom=0.12,
            wspace=0.1,
            left=0.04, 
            right=0.98,
            )
    
    if save:
        output = "fig_dci_pops_iso.png"
        fig.savefig(output, dpi=300)
        print(output)
        
    plt.show()

