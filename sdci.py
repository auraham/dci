# sdci.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import os, sys

from rocket.moea.nds import vfns

from scipy.spatial.distance import cdist

def get_grid_locs(objs, lb, hs):
    """
    Return a (pop_size, m_objs) matrix with the location of each objective vector
    within the grid according to Eq. (5) in [Li14].
    
    Input
    objs        (pop_size, m_objs) obj matrix
    lb          (m_objs, ) lower boundaries of the grid, where lb[k] is the lower boundary of the k-th dimension (objective)
    hs          (m_objs, ) hyperbox size, where hs[k] is the hyperbox size in the k-th dimension (objective)
    
    Output
    locs        (pop_size, m_objs) location matrix
    """
    
    pop_size, m_objs = objs.shape
    
    # reshape
    lb_row = lb.copy().reshape((1, m_objs))
    hs_row = hs.copy().reshape((1, m_objs))
    
    locs = np.floor((objs - lb_row) / hs_row)       # (pop_size, m_objs), Eq (5)

    locs = np.array(locs, dtype=int)                # cast to int
    
    return locs.copy()


def get_contribution_degree(locs_pop, hyperbox):
    """
    
    Input
    locs_pop        (pop_size, m_objs) int matrix, location of the pop in the grid
    hyperbox        (m_objs, ) tuple, location of the hyperbox in the grid
    
    """
    
    # contribution degree
    cd = 0 
    
    pop_size, m_objs = locs_pop.shape
    
    # cast hyperbo
    h_row = np.array(hyperbox, dtype=float).reshape((1, m_objs))
    
    # cast pop
    pop = np.array(locs_pop, dtype=float)
    
    # compute distance between pop and hyperbox
    dist = cdist(h_row, pop, "euclidean")           # (1, pop_size)     Eq (6) in [Li14]
    
    min_dist = dist.min()                           # D(P, h), Eq (7) in [Li14]
    
    # compute contribution degree
    thresh = np.sqrt(m_objs + 1)
    
    if min_dist < thresh:
        
        cd = 1 - ((min_dist**2) / (m_objs+1)) 
    else:
        
        cd = 0
        
    return cd
        

def get_grid_dims(z_ideal, z_nadir, divs):
    """
    Return the size of hyperbox and the boundaries of the grid
    
    Input
    z_ideal     (m_objs, ) array, ideal vector
    z_nadir     (m_objs, ) array, nadir vector
    divs        int, number of divisions of the grid
    
    Output
    lb          (m_objs, ) array, lower boundaries of the grid
    ub          (m_objs, ) array, upper boundaries of the grid
    hs          float, size of hyperbox
    """
    
    lb = z_ideal.copy()
    
    ub = z_nadir + ((z_nadir - z_ideal)/(2*divs))
    
    hs = (ub - lb) / divs
    
    return lb.copy(), ub.copy(), hs 
    

def get_mask(indices, front):
    """
    Compute the number of elements from indices in front
    
    Input
    indices     indices from a pop in merge_pop, ex
    
                indices = [0, ..., 90]          (pop 0 in merged_pops) 
                indices = [91, ..., 181]        (pop 1 in merged_pops) 
                indices = [182, ..., 272]       (pop 2 in merged_pops) 
                indices = [273, ..., 363]       (pop 3 in merged_pops) 
    
    front       indices in the best front
    
    Output
    mask        (n_indices, ) boolean array
                mask[i] = True means that indices[ind] is in best_front
                
                Consider the third pop. Two arrays are defined:
                
                    indices = [182, ..., 272]
                    i       = [0, ..., 91]
                    
                So, indices[i] correspond to i in the loop
                
                for i, ind in enumerate(indices):
                    if ind in front:
                        mask[i] = True
    
    """
    
    mask = np.zeros(indices.shape, dtype=bool)  
    
    count = 0
    
    # i:    index for mask    (eg, 0...90)
    # ind:  index for indices (eg. 91...181)
    for i, ind in enumerate(indices):
        
        if ind in front:
            #count+=1
            mask[i] = True
            
            
    #return count
    return mask.copy()

def sdci(pops, divs, procs=4, debug=False):
    """
    Compute the DCI indicator.
    
    
    Input
    pops        list of (pop_size, m_objs) objs matrices
    divs        int, number of divisions of the grid
    procs       int, number of processes for nds
    
    Output
    dci_vals    (n_pops, ) dci values
    """
    
    # get nds solutions
    merged_pops = np.vstack(pops)
    fronts, ranks = vfns(merged_pops, procs)
    best_front = np.array(fronts[0], dtype=int)
    objs_nds = merged_pops[best_front]
    
    # added to sdci
    # get reference vectors
    z_ideal = objs_nds.min(axis=0)
    z_nadir = objs_nds.max(axis=0)
    
    # grid properties
    lb, ub, hs = get_grid_dims(z_ideal, z_nadir, divs)
    
    # debug
    if debug:
        print("lb: %s, ub: %s, hs: %s" % (lb, ub, hs))
    

    # get locations of pops in grid
    locs_pops = []
    
    for objs_pop in pops:
        
        # (pop_size, m_objs) int matrix, it contains the location of
        # the obj vectors in objs_pop within the grid
        locs_pop = get_grid_locs(objs_pop, lb, hs)
        
        # add it to the list
        locs_pops.append(locs_pop.copy())
    
    # get nds location
    locs_nds = get_grid_locs(objs_nds, lb, hs)
    
    # get nds hyperboxes
    # we use a dictionary for efficient lookup and deletion of duplicate rows in locs_nds
    hyperboxes = {}
    
    for hb_loc in locs_nds:
        
        # cast from np.array to tuple
        key = tuple(hb_loc)
        
        if not key in hyperboxes:
            
            # hyperboxes[loc] contains the number of solutions in the
            # hyperbox defined by loc (although this counter is not necessary for computing DCI)
            hyperboxes[key] = 1         
        
        else:
            
            # increment counter
            hyperboxes[key] += 1
            
            
    
    # compute contribution degree
    n_pops = len(locs_pops)
    n_hyperboxes = len(hyperboxes)
    
    cd_matrix = np.zeros((n_hyperboxes, n_pops))
    
    for row, hyperbox in enumerate(hyperboxes):
        
        # counters
        start, end = 0, 0
        
        for col, locs_pop in enumerate(locs_pops):
            
            # -- added to sdci --
            
            # next batch
            start = end
            end = end + locs_pop.shape[0]
            
            # indices for locs_pop
            indices = np.arange(start, end, dtype=int)
            
            # how many indices (solutions) from locs_pop are in the best front?
            mask = get_mask(indices, best_front)
            
            # filter pop
            nds_locs_pop = locs_pop[mask]
            
            if mask.sum() > 0:
                
                # compute contribution degree from nds_locs_pop
                cd_matrix[row, col] = get_contribution_degree(nds_locs_pop, hyperbox)
            
            else:
                
                # define cd as zero
                cd_matrix[row, col] = 0
            
            # -------------------
            
            # debug
            if debug:
                print("CD(P[%d], h=%s): %.4f" % (col, hyperbox, cd_matrix[row, col]))
            
        # debug
        if debug:
            print("---")
        
    # compute dci
    by_col = 0
    dci_vals = cd_matrix.sum(axis=by_col) / n_hyperboxes
    
    return dci_vals.copy()
    
        

def sdci_debug3(pops, divs, procs=4, debug=False):
    """
    as sdci(), but every solution is involved for computing the contribution, not only nds solutions
    this shows the importance of using nds solutions only for computing the contribution
    Compute the DCI indicator.
    
    
    Input
    pops        list of (pop_size, m_objs) objs matrices
    divs        int, number of divisions of the grid
    procs       int, number of processes for nds
    
    Output
    dci_vals    (n_pops, ) dci values
    """
    
    # get nds solutions
    merged_pops = np.vstack(pops)
    fronts, ranks = vfns(merged_pops, procs)
    best_front = np.array(fronts[0], dtype=int)
    objs_nds = merged_pops[best_front]
    
    # added to sdci
    # get reference vectors
    z_ideal = objs_nds.min(axis=0)
    z_nadir = objs_nds.max(axis=0)
    
    # grid properties
    lb, ub, hs = get_grid_dims(z_ideal, z_nadir, divs)
    
    # debug
    if debug:
        print("lb: %s, ub: %s, hs: %s" % (lb, ub, hs))
    

    # get locations of pops in grid
    locs_pops = []
    
    for objs_pop in pops:
        
        # (pop_size, m_objs) int matrix, it contains the location of
        # the obj vectors in objs_pop within the grid
        locs_pop = get_grid_locs(objs_pop, lb, hs)
        
        # add it to the list
        locs_pops.append(locs_pop.copy())
    
    # get nds location
    locs_nds = get_grid_locs(objs_nds, lb, hs)
    
    # get nds hyperboxes
    # we use a dictionary for efficient lookup and deletion of duplicate rows in locs_nds
    hyperboxes = {}
    
    for hb_loc in locs_nds:
        
        # cast from np.array to tuple
        key = tuple(hb_loc)
        
        if not key in hyperboxes:
            
            # hyperboxes[loc] contains the number of solutions in the
            # hyperbox defined by loc (although this counter is not necessary for computing DCI)
            hyperboxes[key] = 1         
        
        else:
            
            # increment counter
            hyperboxes[key] += 1
            
            
    
    # compute contribution degree
    n_pops = len(locs_pops)
    n_hyperboxes = len(hyperboxes)
    
    cd_matrix = np.zeros((n_hyperboxes, n_pops))
    
    for row, hyperbox in enumerate(hyperboxes):
        
        # counters
        start, end = 0, 0
        
        for col, locs_pop in enumerate(locs_pops):
            
            # -- added to sdci_debug3 --
            
            # compute contribution degree from nds_locs_pop
            cd_matrix[row, col] = get_contribution_degree(locs_pop, hyperbox)
            
            
            """
            # next batch
            start = end
            end = end + locs_pop.shape[0]
            
            # indices for locs_pop
            indices = np.arange(start, end, dtype=int)
            
            # how many indices (solutions) from locs_pop are in the best front?
            mask = get_mask(indices, best_front)
            
            # filter pop
            nds_locs_pop = locs_pop[mask]
            
            if mask.sum() > 0:
                
                # compute contribution degree from nds_locs_pop
                cd_matrix[row, col] = get_contribution_degree(nds_locs_pop, hyperbox)
            
            else:
                
                # define cd as zero
                cd_matrix[row, col] = 0
            """
            # -------------------
            
            # debug
            if debug:
                print("CD(P[%d], h=%s): %.4f" % (col, hyperbox, cd_matrix[row, col]))
            
        # debug
        if debug:
            print("---")
        
    # compute dci
    by_col = 0
    dci_vals = cd_matrix.sum(axis=by_col) / n_hyperboxes
    
    return dci_vals.copy()
    
        
    
def sdci_debug(pops, z_ideal, z_nadir, divs, procs=4, debug=False):
    """
    Similar to sdci(), but here you can pass z_ideal and z_nadir
    
    Compute the DCI indicator.
    
    
    Input
    pops        list of (pop_size, m_objs) objs matrices
    divs        int, number of divisions of the grid
    procs       int, number of processes for nds
    
    Output
    dci_vals    (n_pops, ) dci values
    """
    
    # get nds solutions
    merged_pops = np.vstack(pops)
    fronts, ranks = vfns(merged_pops, procs)
    best_front = np.array(fronts[0], dtype=int)
    objs_nds = merged_pops[best_front]
    
    # added to sdci
    # get reference vectors
    #z_ideal = objs_nds.min(axis=0)
    #z_nadir = objs_nds.max(axis=0)
    
    # grid properties
    lb, ub, hs = get_grid_dims(z_ideal, z_nadir, divs)
    
    # debug
    if debug:
        print("lb: %s, ub: %s, hs: %s" % (lb, ub, hs))
    

    # get locations of pops in grid
    locs_pops = []
    
    for objs_pop in pops:
        
        # (pop_size, m_objs) int matrix, it contains the location of
        # the obj vectors in objs_pop within the grid
        locs_pop = get_grid_locs(objs_pop, lb, hs)
        
        # add it to the list
        locs_pops.append(locs_pop.copy())
    
    # get nds location
    locs_nds = get_grid_locs(objs_nds, lb, hs)
    
    # get nds hyperboxes
    # we use a dictionary for efficient lookup and deletion of duplicate rows in locs_nds
    hyperboxes = {}
    
    for hb_loc in locs_nds:
        
        # cast from np.array to tuple
        key = tuple(hb_loc)
        
        if not key in hyperboxes:
            
            # hyperboxes[loc] contains the number of solutions in the
            # hyperbox defined by loc (although this counter is not necessary for computing DCI)
            hyperboxes[key] = 1         
        
        else:
            
            # increment counter
            hyperboxes[key] += 1
            
            
    
    # compute contribution degree
    n_pops = len(locs_pops)
    n_hyperboxes = len(hyperboxes)
    
    cd_matrix = np.zeros((n_hyperboxes, n_pops))
    
    for row, hyperbox in enumerate(hyperboxes):
        
        # counters
        start, end = 0, 0
        
        for col, locs_pop in enumerate(locs_pops):
            
            # -- added to sdci --
            
            # next batch
            start = end
            end = end + locs_pop.shape[0]
            
            # indices for locs_pop
            indices = np.arange(start, end, dtype=int)
            
            # how many indices (solutions) from locs_pop are in the best front?
            mask = get_mask(indices, best_front)
            
            # filter pop
            nds_locs_pop = locs_pop[mask]
            
            if mask.sum() > 0:
                
                # compute contribution degree from nds_locs_pop
                cd_matrix[row, col] = get_contribution_degree(nds_locs_pop, hyperbox)
            
            else:
                
                # define cd as zero
                cd_matrix[row, col] = 0
            
            # -------------------
            
            # debug
            if debug:
                print("CD(P[%d], h=%s): %.4f" % (col, hyperbox, cd_matrix[row, col]))
            
        # debug
        if debug:
            print("---")
        
    # compute dci
    by_col = 0
    dci_vals = cd_matrix.sum(axis=by_col) / n_hyperboxes
    
    return dci_vals.copy()
    
        
    
def sdci_debug2(pops, z_ideal, z_nadir, divs, procs=4, debug=False):
    """
    Similar to sdci_debug(), but non-nds solutions are also considered
    
    Compute the DCI indicator.
    
    
    Input
    pops        list of (pop_size, m_objs) objs matrices
    divs        int, number of divisions of the grid
    procs       int, number of processes for nds
    
    Output
    dci_vals    (n_pops, ) dci values
    """
    
    # get nds solutions
    merged_pops = np.vstack(pops)
    fronts, ranks = vfns(merged_pops, procs)
    best_front = np.array(fronts[0], dtype=int)
    objs_nds = merged_pops[best_front]
    
    # added to sdci
    # get reference vectors
    #z_ideal = objs_nds.min(axis=0)
    #z_nadir = objs_nds.max(axis=0)
    
    # grid properties
    lb, ub, hs = get_grid_dims(z_ideal, z_nadir, divs)
    
    # debug
    if debug:
        print("lb: %s, ub: %s, hs: %s" % (lb, ub, hs))
    

    # get locations of pops in grid
    locs_pops = []
    
    for objs_pop in pops:
        
        # (pop_size, m_objs) int matrix, it contains the location of
        # the obj vectors in objs_pop within the grid
        locs_pop = get_grid_locs(objs_pop, lb, hs)
        
        # add it to the list
        locs_pops.append(locs_pop.copy())
    
    # get nds location
    locs_nds = get_grid_locs(objs_nds, lb, hs)
    
    # get nds hyperboxes
    # we use a dictionary for efficient lookup and deletion of duplicate rows in locs_nds
    hyperboxes = {}
    
    for hb_loc in locs_nds:
        
        # cast from np.array to tuple
        key = tuple(hb_loc)
        
        if not key in hyperboxes:
            
            # hyperboxes[loc] contains the number of solutions in the
            # hyperbox defined by loc (although this counter is not necessary for computing DCI)
            hyperboxes[key] = 1         
        
        else:
            
            # increment counter
            hyperboxes[key] += 1
            
            
    
    # compute contribution degree
    n_pops = len(locs_pops)
    n_hyperboxes = len(hyperboxes)
    
    cd_matrix = np.zeros((n_hyperboxes, n_pops))
    
    for row, hyperbox in enumerate(hyperboxes):
        
        # counters
        start, end = 0, 0
        
        for col, locs_pop in enumerate(locs_pops):
            
            # -- added to sdci_debug2 --
            
            # compute contribution degree
            cd_matrix[row, col] = get_contribution_degree(locs_pop, hyperbox)
            
            # -------------------
            
            # debug
            if debug:
                print("CD(P[%d], h=%s): %.4f" % (col, hyperbox, cd_matrix[row, col]))
            
        # debug
        if debug:
            print("---")
        
    # compute dci
    by_col = 0
    dci_vals = cd_matrix.sum(axis=by_col) / n_hyperboxes
    
    return dci_vals.copy()
    
        
    
    
    
