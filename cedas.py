# cedas.py
from __future__ import print_function
import numpy as np
from scipy.linalg import norm
import matplotlib.pyplot as plt

eps = 1.0e-20


def cedas_slow(objs, s):
    """
    Contraction and Expansion of Dominance Area of Solutions
    
    Input
    objs        (pop_size, m_objs) array of objectives
    s           double, cedas parameter
                s = 0.5 results in conventional Pareto dominance (ie, there is no transormation)
    
    Output
    objs_t      (pop_size, m_objs) array of 'relaxed' objectives
    """
    
    pop_size, m_objs = objs.shape
    objs_t = np.zeros((pop_size, m_objs))
    
    if isinstance(s, (int, float)):
        # convert it to an array
        # thus, there is a S_i value for each objective f_i
        s = np.array([ s for i in range(m_objs)])
    
    
    for i in range(pop_size):
        
        f   = objs[i]                   # (m_objs, ) array
        r   = norm(f)                   # scalar
        f_t = np.zeros((m_objs), )      # transformed array
        
        # adjust norm
        r_omega = r
        if r == 0:
            r_omega = eps
        
        for m in range(m_objs):
            
            fi = f[m]                       # i-th objective value
            omega = np.arccos(fi/r_omega)  
            phi = s[m]*np.pi                # adjust for i-th objective value
            
            sin_phi = np.sin(phi)
            
            # adjust sin(phi)
            if sin_phi == 0:
                sin_phi = eps
                
            # i-th obj transformation
            f_t[m] = (r*np.sin(omega + phi)) / sin_phi
        
        # save vector
        objs_t[i,:] = f_t.copy()
    
    return objs_t.copy()
    
    
if __name__ == "__main__":
        
    objs = np.array([
                [8.0, 4.0],
                [3.0, 5.0],
                [4.0, 3.0]])

    objs_1 = cedas_slow(objs, s=0.25)
    objs_2 = cedas_slow(objs, s=0.50)
    objs_3 = cedas_slow(objs, s=0.75)


    import os, sys, argparse

    np.seterr(all='raise')

    # add lab's path to sys.path
    lab_path = os.path.abspath("../../")
    sys.path.insert(0, lab_path)

    from rocket.plot import plot_pops

    pops = (objs, objs_1, objs_2, objs_3)
    labels = ("original", "0.25", "0.50", "0.75")
    #plot_pops(pops, labels, loc="upper left")

