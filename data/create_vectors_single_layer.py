# create_vectors_single_layer.py
from __future__ import print_function
from reference_vectors import create_reference_vectors
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from load_rcparams import load_rcparams

def replace_zero_weigths(weights, eps=0.00001):
    """
    Replace zero weights given an input matrix
    
    Input
    weights     (pop_size, m_objs) array, weights matrix
    eps         new weight value
    
    Output
    W           (pop_size, m_objs) array, new weights matrix
    """
    
    # replace zero weights
    W           = weights.copy()
    to_keep     = weights == 0
    W[to_keep]  = eps           #0.00001               # recommended value [Ishibuchi16]
    
    return W.copy()
    
def norm_v1(objs, lb, ub):
    """
    Normalize objs while preserving the scale proportion among objectives
    """
    
    pop_size, m_objs = objs.shape
    
    by_col = 0
    
    # get absolute maximum
    z_nadir = objs.max(axis=by_col)
    z_ideal = objs.min(axis=by_col)
    
    eps = 1e-50
    div = (z_nadir - z_ideal).reshape((1, m_objs)) + eps

    factor = (ub - lb) + lb
    
    norm_a = (objs - z_ideal) / div     # normalizacion comun
    norm_b = (norm_a * (ub - lb)) + lb
    
    return norm_b.copy()
    

if __name__ == "__main__":
    
    # (dims, spacing h1, spacing h2, is_for_testing)
    params = (
            
            # for H=2
            ( 3, 13, 0),     # 
            #( 4, 7, 0),      # 
            #( 5, 6, 0),      # 
            #( 6, 4, 0),      # 
            #( 7, 4, 0),      # 
            #( 8, 3, 0),      # 
            #(10, 3, 0),      #
             
            )
            
    scales = (
            ("a", 0, 1.0), 
            ("b", 0.05, 0.9),
            ("c", 0.1, 0.8),
            ("d", 0.2, 0.6),
            )
            
    
    for dims, h1, h2 in params:
            
    
        # create first layer
        w = create_reference_vectors(dims, h1)
        
        layer_a_count = w.shape[0]
        layer_b_count = 0

        
        for label, lb, ub in scales:
        
            #w = w * (ub - lb) + lb         # no es correcto
            #import ipdb; ipdb.set_trace()  
            w = norm_v1(w.copy(), lb, ub)   # si es correcto
            
            # debug
            print("min: ", " ".join([ "%.5f" % v for v in w.min(axis=0)]) )
            print("max: ", " ".join([ "%.5f" % v for v in w.max(axis=0)]) )
            
        
            # weights with zeros
            filename = "weights_m_%d_%s_zeros.txt" % (dims, label)
            
            # uncomment to save
            np.savetxt(filename, w, fmt="%.10f", delimiter="\t", newline="\t\n")
            print("creating", filename)
            
            # weights with no zeros
            nw = replace_zero_weigths(w)
            filename = "weights_m_%d_%s.txt" % (dims, label)
            
            # uncomment to save
            np.savetxt(filename, nw, fmt="%.10f", delimiter="\t", newline="\t\n")
            print("creating", filename)
            print("count: %d" % layer_a_count)
            
            
        print("")
        
        
    load_rcparams((16, 4))
    fig = plt.figure()
    axes = [fig.add_subplot(141, projection="3d"),
            fig.add_subplot(142, projection="3d"),
            fig.add_subplot(143, projection="3d"),
            fig.add_subplot(144, projection="3d"),
            ]

    for i, label in enumerate(("a", "b", "c", "d")):
        
        filename = "weights_m_3_%s.txt" % label
        w = np.genfromtxt(filename)
        
        ax = axes[i]
        ax.plot(w[:, 0], w[:, 1], w[:, 2], marker="o", color="#74CE74", ls="none")
        
        ax.view_init(30, 45)
        ax.set_xlim(0, 1.05)
        ax.set_ylim(0, 1.05)
        ax.set_zlim(0, 1.05)


    plt.subplots_adjust(left=0.05, right=0.95)
    plt.show()
