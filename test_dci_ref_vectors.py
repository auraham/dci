# test_dci_ref_vectors.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import rcParams
from mpl_toolkits.mplot3d import Axes3D
import os, sys
np.seterr(all='raise')

from vfns import vfns
from dci import dci
from plotting import load_rcparams, plot_pops, plot_iso

if __name__ == "__main__":
    
    
    # load objs
    objs_a = np.genfromtxt("data/weights_m_3_a_zeros.txt")
    objs_b = np.genfromtxt("data/weights_m_3_b_zeros.txt")
    objs_c = np.genfromtxt("data/weights_m_3_c_zeros.txt")
    objs_d = np.genfromtxt("data/weights_m_3_d_zeros.txt")
    objs_e = objs_a*1.5
    
    # make a list of pops
    pops = (objs_a, objs_b, objs_c, objs_d, objs_e)
    labels = ("$P_1$", "$P_2$", "$P_3$", "$P_4$", "$P_5$")
    
    # get non-dominated solutions using vfns
    merged_pops = np.vstack(pops)
    fronts, ranks = vfns(merged_pops, procs=4)
    best_front = np.array(fronts[0], dtype=int)
    objs_nds = merged_pops[best_front]
    
    # plot pops
    plot_pops(pops, labels, merged_pops, best_front, save=True)
    
    # get reference vectors from non-dominated solutions
    z_ideal = objs_nds.min(axis=0)
    z_nadir = objs_nds.max(axis=0)
    
    # test different points
    z_nadir_pool = (z_nadir.copy(), np.array([3, 3, 3]), np.array([5, 5, 5]))
    
    for z_nadir in z_nadir_pool:
        
        print("z_nadir: %s" % z_nadir)
        
        # compute dci
        divs = 19
        dci_vals = dci(pops, z_ideal, z_nadir, divs, procs=4, debug=False)
        
        # grid properties
        lb, ub, hs = get_grid_dims(z_ideal, z_nadir, divs)
        print("lb: %s, ub: %s, hs: %s" % (lb, ub, hs))

        # print results
        for i, val in enumerate(dci_vals):
            pop_label = labels[i].replace("$", "")
            print("DCI(%s): %.4f" % (pop_label, dci_vals[i]))

        print("")
