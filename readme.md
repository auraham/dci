# Diversity comparison indicator

We describe a diversity indicator proposed by Li et al. [Li14], called *Diversity Comparison Indicator* (DCI). We provide a Python implementation and show an example of how to use it.



## Intro

*[describe method here]*



## Scripts

| Name           | Description                                                  |
| -------------- | ------------------------------------------------------------ |
| `main.py`      | This scripts shows how to use the`dci` function to compute DCI. |
| `dci.py`       | Implementation of DCI.                                       |
| `vfns.py`      | Implementation of Very Fast Non-Dominated Sorting (VFNS) [Smutnicki], a parallel approach for computing the non-dominated sorting. |
| `dominance.py` | Dominance functions.                                         |
| `plotting.py`  | Plotting functions.                                          |



## First example

Run `main.py` to create this figure:



![](fig_dci_pops.png)



This figure is similar to that published in [Li14]. We have employed this figure to test our implementation. You can compute DCI as follows:

```python
# compute dci
divs = 19
dci_vals = dci(pops, z_ideal, z_nadir, divs, procs=4)
```

where:

- `pops` is a list of matrices (populations) with objective vectors.
- `z_ideal` is the ideal vector.
- `z_nadir` is the Nadir vector.
- `divs` is the number of divisions of the grid for computing DCI.
- `procs` is the number of processes for computing VFNS.

In the following, we will describe the content of `main.py`.

First, load the sample populations:

```python
# load objs
objs_a = np.genfromtxt("data/weights_m_3_a_zeros.txt")
objs_b = np.genfromtxt("data/weights_m_3_b_zeros.txt")
objs_c = np.genfromtxt("data/weights_m_3_c_zeros.txt")
objs_d = np.genfromtxt("data/weights_m_3_d_zeros.txt")
objs_e = objs_a*1.5
```

and store them in a list:

```python
# make a list of pops
pops = (objs_a, objs_b, objs_c, objs_d, objs_e)
labels = ("$P_1$", "$P_2$", "$P_3$", "$P_4$", "$P_5$")
```

Then, merge the populations together in a single matrix to get the non-dominated solutions among all the populations. Here, we use `vfns` with `procs=4` processes. `best_front` contains the indices of the non-dominated solutions in `merged_pops`. Finally, we keep the objective vectors of the non-dominated solutions by using `merged_pops[best_front]`.

```python
# get non-dominated solutions using vfns
merged_pops = np.vstack(pops)
fronts, ranks = vfns(merged_pops, procs=4)
best_front = np.array(fronts[0], dtype=int)
objs_nds = merged_pops[best_front]
```

From these solutions, we estimate the values of `z_ideal` and `z_nadir` as follows:

```python
# get reference vectors from non-dominated solutions
z_ideal = objs_nds.min(axis=0)
z_nadir = objs_nds.max(axis=0)
```

Finally, compute DCI:

```python
# compute dci
divs = 19
dci_vals = dci(pops, z_ideal, z_nadir, divs, procs=4, debug=False)
```

This is the output:

```python
DCI(P_1) = 0.8000
DCI(P_2) = 0.7816
DCI(P_3) = 0.7026
DCI(P_4) = 0.4026
DCI(P_5) = 0.0000
```

From these results, the first population (`objs_a`) has the best relative diversity considering the set of populations (`pops`).



## Second example

In this case, we will show the importance of the `z_nadir` point. It turns out that, as this point moves away from the origin (i.e., the Pareto front), the DCI becomes meaningless. Run `test_dci_ref_vectors.py` to show the example. This scripts computes the diversity indicator using three settings of `z_nadir`. This is the output:

```
z_nadir: [1. 1. 1.]
lb: [0. 0. 0.], ub: [1.02631579 1.02631579 1.02631579], hs: [0.05401662 0.05401662 0.05401662]
DCI(P_1): 0.8000
DCI(P_2): 0.7816
DCI(P_3): 0.7026
DCI(P_4): 0.4026
DCI(P_5): 0.0000
```

In this case,  we use the same setting as in the first experiment,`z_nadir: [1. 1. 1.]`. The variables `lb`, `ub`, and `hs` denote the lower bound, upper bound, and hyperbox size of the grid required for DCI, respectively. In this case, `P_1` has the best relative diversity.

```
z_nadir: [3 3 3]
lb: [0. 0. 0.], ub: [3.07894737 3.07894737 3.07894737], hs: [0.16204986 0.16204986 0.16204986]
DCI(P_1): 1.0000
DCI(P_2): 0.9808
DCI(P_3): 0.9038
DCI(P_4): 0.5769
DCI(P_5): 0.3654
```

In this case, the Nadir vector is `z_nadir: [3 3 3]`. The population `P_1` is still the best. However, notice that the DCI value of `P_5` increased.


```
z_nadir: [5 5 5]
lb: [0. 0. 0.], ub: [5.13157895 5.13157895 5.13157895], hs: [0.2700831 0.2700831 0.2700831]
DCI(P_1): 1.0000
DCI(P_2): 1.0000
DCI(P_3): 0.9605
DCI(P_4): 0.8816
DCI(P_5): 0.8289
```

Finally, we consider `z_nadir: [5 5 5]`.  Notice that the best populations are `P_1` and `P_2` under this setting. However, `P_1` should be considered the best one. Also, the DCI values of the other populations increased in this case. As you see, it is important to define `z_nadir` properly when using DCI. For instance, if some populations contain *dominance resistance solutions* (DRS), then the estimation of the Nadir vector from non-domianted solutions could be ineffective. Consequently, the outcome of DCI could be missleading.



## Third example

Now, we will modify `dci` to handle approximations with irregular scaling/range. In the previous examples, the range of every objective function is [0, 1]. That is, the scale is the same for each approximation. However, when such a range varies among objectives, we must normalize the approximations before computing the DCI. Here, we will introduce `dci_norm`. Let us compare both `dci` and `dci_norm` before showing the example:

```
# dci
1. Employ z_ideal and z_nadir as defined by the user
2. Compute grid dimensions (lb, ub, hs)
3. Filter solutions outside the grid
4. Obtain non-dominated solutions
5. Identify non-empty hyperboxes from non-dominated solutions
6. Compute DCI
```

```
# dci_norm
1*. Obtain non-dominated solutions
2*. Define z_ideal as z_nadir from non-dominated solutions
3*. Normalize populations using z_ideal and z_nadir
4. Compute grid dimensions (lb, ub, hs)
5. Filter solutions outside the grid
6. Identify non-empty hyperboxes from non-dominated solutions
8. Compute DCI
```

Here, `*` means our additional steps.

This snippet shows the relevant parts of `dci_norm`:



```python
def dci_norm(pops, divs, procs=4, debug=False):
    
    # 1. get non-dominated solutions
    merged_pops = np.vstack(pops)
    fronts, ranks = vfns(merged_pops, procs)
    best_front = np.array(fronts[0], dtype=int)
    objs_nds = merged_pops[best_front]
    
    # 2. obtain z_ideal, z_nadir from objs_nds
    z_ideal = objs_nds.min(axis=0)
    z_nadir = objs_nds.max(axis=0)
    
    # 3. normalize pops and objs_nds
    norm_pops = normalize_pops(pops, z_ideal, z_nadir)
    norm_objs_nds = normalize(objs_nds, z_ideal, z_nadir)
    
    # 4. compute grid dimesions
    m_objs = norm_objs_nds.shape[1]
    ref_ideal = np.zeros((m_objs, ))   # note this change
    ref_nadir = np.ones((m_objs, ))    # note this changes
    lb, ub, hs = get_grid_dims(ref_ideal, ref_nadir, divs)
    
    # remove solutions out of the grid
    # this step must be performed before calling get_grid_locs()
    pops = filter_pops(norm_pops, lb, ub, debug)
    objs_nds = filter_pop(norm_objs_nds, lb, ub, debug)
    
    # get locations of pops in grid
    locs_pops = []
    
    for objs_pop in pops:
        
        # (pop_size, m_objs) int matrix, it contains the location of
        # the obj vectors in objs_pop within the grid
        locs_pop = get_grid_locs(objs_pop, lb, hs)
        
        # add it to the list
        locs_pops.append(locs_pop.copy())
    
    # get nds location
    locs_nds = get_grid_locs(objs_nds, lb, hs)
    
    # get non-empty hyperboxes
    hyperboxes = {}
    
    for hb_loc in locs_nds:
        
        # cast from np.array to tuple
        key = tuple(hb_loc)
        
        if not key in hyperboxes:
            hyperboxes[key] = 1         
        
    # compute contribution degree
    n_pops = len(locs_pops)
    n_hyperboxes = len(hyperboxes)
    cd_matrix = np.zeros((n_hyperboxes, n_pops))
    
    for row, hyperbox in enumerate(hyperboxes):
        
        for col, locs_pop in enumerate(locs_pops):
            
            # compute contribution degree
            cd_matrix[row, col] = get_contribution_degree(locs_pop, hyperbox)
        
    # compute dci
    by_col = 0
    dci_vals = cd_matrix.sum(axis=by_col) / n_hyperboxes
    
    return dci_vals.copy()
```

Notice that `dci_norm` should return the same output of `dci`. To test it, run `test_dci_norm.py`. This is the output:

```
DCI(P_1) = 0.8000
DCI(P_2) = 0.7816
DCI(P_3) = 0.7026
DCI(P_4) = 0.4026
DCI(P_5) = 0.0000
```

Here, we computed the `dci_norm` values using the same populations of the first example. As it can be seen, the values are the same as those obtained by `dci`. Now, run `test_dci_norm_irregular.py`. This script generates the following figure:

![](fig_dci_irregular_pops.png)

Notice the range of each objective. The populations were scaled as follows:

```python
# adjust range
m_objs = 3
scales = [1, 10, 100]
new_pops = []

for pop in pops:

    new_pop = pop.copy()

    for m in range(m_objs):
    	new_pop[:, m] = pop[:, m] * scales[m]

new_pops.append(new_pop.copy())
```

Thus:

- the first objective is in the range [0, 1],
- the second objective is in the range [0, 10], and
- the third objective is in the range [0, 100].



Before computing DCI, `dci_norm` normalizes the input populations by using `z_ideal` and `z_nadir`:



```python
# 1. get non-dominated solutions
merged_pops = np.vstack(pops)
fronts, ranks = vfns(merged_pops, procs)
best_front = np.array(fronts[0], dtype=int)
objs_nds = merged_pops[best_front]

# 2. obtain z_ideal, z_nadir from objs_nds
z_ideal = objs_nds.min(axis=0)
z_nadir = objs_nds.max(axis=0)

# 3. normalize pops and objs_nds
norm_pops = normalize_pops(pops, z_ideal, z_nadir)
norm_objs_nds = normalize(objs_nds, z_ideal, z_nadir)
```

Then, we obtain the properties of the grid:

```python
# 4. compute grid dimesions
m_objs = norm_objs_nds.shape[1]
ref_ideal = np.zeros((m_objs, ))
ref_nadir = np.ones((m_objs, ))
lb, ub, hs = get_grid_dims(ref_ideal, ref_nadir, divs)
```

Later, we remove those solutions outside of the grid. This step is important and should be called before assigning solutions in the grid (i.e., before calling `get_grid_locs()`):

```python
# remove solutions out of the grid
pops = filter_pops(norm_pops, lb, ub, debug)
objs_nds = filter_pop(norm_objs_nds, lb, ub, debug)
```

Now, we can locate the solutions within the grid:

```python
# get locations of pops in grid
locs_pops = []
    
for objs_pop in pops:
        
    # (pop_size, m_objs) int matrix, it contains the location of
    # the obj vectors in objs_pop within the grid
    locs_pop = get_grid_locs(objs_pop, lb, hs)
        
    # add it to the list
    locs_pops.append(locs_pop.copy())
    
# get nds location
locs_nds = get_grid_locs(objs_nds, lb, hs)
```

The rest of the code of `dci_norm` is similar to `dci`. We must identify non-empty hyperboxes and compute the contribution degree for each one.



## Fourth example

Now, let us compare `dci` and `dci_norm` using a set of populations saved from a experiment. These six populations were produced when evaluating DTLZ2 with ten objecitves by using six scalarizing functions: WS, TE, ASF, PBI, IPBI and VADS. The goal of `dci` and `dci_norm` is to determine which of them has the better relative diversity. Empirically, the population based on PBI should achieve the best performance. Run `test_dci_dtlz2.py` and `test_dci_norm_dtlz2.py` to check the outcome of both indicators.

```
In [1]: %run test_dci_dtlz2.py
...
The number of non-dominated solutions is 1145
This pop has 1145 in the grid (original size: 1145)
The number of boxes where all solutions locate is 468
DCI(ws) 	= 0.0707
DCI(te) 	= 0.3737
DCI(asf) 	= 0.1976
DCI(pbi) 	= 0.5507
DCI(ipbi) 	= 0.1474
DCI(vads) 	= 0.5488
```

```
In [2]: %run test_dci_norm_dtlz2.py 
...
The number of non-dominated solutions is 1145
This pop has 1145 in the grid (original size: 1145)
The number of boxes where all solutions locate is 468
DCI(ws) 	= 0.0707
DCI(te) 	= 0.3737
DCI(asf) 	= 0.1976
DCI(pbi) 	= 0.5507
DCI(ipbi) 	= 0.1474
DCI(vads) 	= 0.5488
```

As it can be seen, the outcome is the same. From this example, the population based in PBI reached the best performance. In this case, we employed `divs = 19` divisions for the grid. However, the authors recommend a lower value, such as `divs = 5` when the number of objectives is ten. Let's try that value. Update both scripts `test_dci_dtlz2.py` and `test_dci_norm_dtlz2.py` to use `divs=3`. This is the output:

```
In [1]: %run test_dci_dtlz2.py                                               
...
The number of non-dominated solutions is 1145
This pop has 1145 in the grid (original size: 1145)
The number of boxes where all solutions locate is 421
DCI(ws) 	= 0.4191
DCI(te) 	= 0.6973
DCI(asf) 	= 0.6212
DCI(pbi) 	= 0.8810    <- tie
DCI(ipbi) 	= 0.6191
DCI(vads) 	= 0.8810    <- tie
```

```
In [2]: %run test_dci_norm_dtlz2.py                                           ...
This pop has 1145 in the grid (original size: 1145)
The number of boxes where all solutions locate is 421
DCI(ws) 	= 0.4191
DCI(te) 	= 0.6973
DCI(asf) 	= 0.6212
DCI(pbi) 	= 0.8810    <- tie
DCI(ipbi) 	= 0.6191
DCI(vads) 	= 0.8810    <- tie
```

The values are the same. However, there is a tie between PBI and VADS. From this example, we can conclude that the performance of DCI depends on the specification of `divs`.



## Log

- `changeset 14:a9e6746a67d1` Function `filter_pops` was added to remove solutions outside the grid before computing the DCI values. This modification was made after comparing the code of DCI (from the authors) against our code.

```python
def filter_pops(pops, lb, ub):
    """
    Remove solutions in pops out of the grid defined by lb and ub.
    
    Input
    pops        list of (pop_size, m_objs) objs matrices
    lb          (m_objs, ) array, lower boundaries of the grid
    ub          (m_objs, ) array, upper boundaries of the grid
    
    Output
    pops        list of (pop_size, m_objs) filtered matrices
    """
    
    new_pops = []
    
    m_objs = lb.shape[0]
    lb_row = lb.reshape((1, m_objs))
    ub_row = ub.reshape((1, m_objs))
    
    for pop in pops:

        # identify solution within the grid
        to_keep = (pop >= lb_row ) & (pop <= ub_row)
        
        # reshape (pop_size, m_objs) -> (pop_size, )
        to_keep = np.all(to_keep, axis=1)
        
        # keep those solutions
        new_pop = pop[to_keep].copy()
        
        # add new pop to the list
        new_pops.append(new_pop.copy())
        
    return new_pops
```

```python
def dci(pops, z_ideal, z_nadir, divs, procs=4, debug=False):
    
    # grid properties
    lb, ub, hs = get_grid_dims(z_ideal, z_nadir, divs)
    
    # new call
    # remove solutions out of the grid
    pops = filter_pops(pops, lb, ub)
    
```

- `changeset 34:d2e0d837981e` This version contains `dci_norm`. This version of `dci` normalizes the input populations before computing the DCI.





```

# dci_norm_cedas
1*. Obtain non-dominated solutions
2*. Define z_ideal as z_nadir from non-dominated solutions
3*. Normalize populations using z_ideal and z_nadir
4. Compute grid dimensions (lb, ub, hs)
5. Filter solutions outside the grid
6. Identify non-empty hyperboxes from non-dominated solutions
8. Compute DCI


1. NDS en pops usando CEDAS

​```
to_keep_cedas = cedas(pops, s=0.3)
objs_nds_cedas = merge_pops[to_keep_cedas]  # objetivos transformados
                                            # solo nos interesan los indices
​```

2. Identify non-dominated solutions with no transformation

​```
objs_nds = merge_pops[to_keep_cedas]        # este subconjunto debe estar libre de DRS
​```

2. Estimar z_nadir con los indices identificados por cedas

​```
z_nadir = objs_nds.max(axis=0)
​```


3. Normalizar pops

...


```





## TODO

- [ ] Check why is this import valid

```python
# test_dci_ref_vectors.py
from dci import get_grid_dims, dci   # get_grid_dims belongs to dci_common.py
```








## References

- [Li14] *Diversity Comparison of Pareto Front Approximations in Many-Objective Optimization.*
- [Smutnicki14] *Very Fast Non-Dominated Sorting*.



