# test_dci_norm_inv_dtlz1.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import rcParams
from mpl_toolkits.mplot3d import Axes3D
import os, sys
np.seterr(all='raise')

from vfns import vfns
from dci import get_grid_dims, dci
from plotting import load_rcparams, plot_pops, plot_iso

def load_objs(fev):
    
    moea_name = "amoead_mss_v4-no-form-contrib_step-25-contrib_window-10-smooth-mean-contrib-fast_no_parent_sdci2-assignment-fast"
    filename = "objs_%s_m_3_run_0_t_25_fev_%s.txt" % (moea_name, fev)
    filepath = os.path.join("sample_pops", "inv-dtlz1", moea_name, "pops", filename)
    
    objs = np.genfromtxt(filepath)
    
    return objs.copy()
    
def vector_str(v):
    
    line = " ".join(["%.10f" % val for val in v])
    return line

if __name__ == "__main__":
    
    # load objs
    objs_a = load_objs("ipbi-2")
    objs_b = load_objs("ipbi-4")
    objs_c = load_objs("ipbi-5")
    objs_d = load_objs("ipbi-6")
    objs_e = load_objs("ipbi-8")
    objs_f = load_objs("pbi-5")
    
    # make a list of pops
    pops = (objs_a, objs_b, objs_c, objs_d, objs_e, objs_f)
    labels = ("ipbi-2", "ipbi-4", "ipbi-5", "ipbi-6", "ipbi-8", "pbi-5")
    
    # get non-dominated solutions using vfns
    merged_pops = np.vstack(pops)
    fronts, ranks = vfns(merged_pops, procs=4)
    best_front = np.array(fronts[0], dtype=int)
    objs_nds = merged_pops[best_front]
    
    # get reference vectors from non-dominated solutions
    z_ideal = objs_nds.min(axis=0)
    z_nadir = objs_nds.max(axis=0)
    
    # print vectors
    print("z_ideal:", vector_str(z_ideal))
    print("z_nadir:", vector_str(z_nadir))
    
    # compute dci
    divs = 19
    dci_vals = dci(pops, z_ideal, z_nadir, divs, procs=4, debug=True)

    # print results
    for i, val in enumerate(dci_vals):
        
        pop_label = labels[i]
        print("DCI(%s) \t= %.4f" % (pop_label, val))

