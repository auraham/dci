# test_dci.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import os, sys

np.seterr(all='raise')

# add lab's path to sys.path
lab_path = os.path.abspath("../../")
sys.path.insert(0, lab_path)

from rocket.plot import load_rcparams, plot_pops
from rocket.moea.nds import vfns
from dci import get_grid_locs, get_contribution_degree, dci

def plot(pops, labels):
    
    load_rcparams((8, 8))
    
    
    fig, ax = plot_pops(pops, labels)
    ax.set_xlim(0, 8)
    ax.set_ylim(0, 8)
    ax.set_xticks([i for i in range(9)])
    ax.set_yticks([i for i in range(9)])
    #ax.set_xticklabels()
    ax.grid()
    plt.show()


if __name__ == "__main__":
    
    objs_a = np.array([
                    [0.4, 6.8],
                    [1.5, 4.5],
                    [2.5, 2.8],
                    [4.2, 2.2],
                    [5.8, 1.2],
                    [7.5, 0.2],
                    ])
                    
    objs_b = np.array([
                    [0.2, 7.52],
                    [0.8, 6.2],
                    [4.9, 1.8],
                    [6.4, 0.6],
                    [7.2, 0.4],
                    [4.4, 5.8],
                    ])
                    
    objs_c = np.array([
                    [1.2, 4.8],
                    [1.9, 3.5],
                    [3.2, 2.6],
                    [3.9, 2.5],
                    [4.6, 2.0],
                    [7.52, 2.1],
                    ])
                    
    
    # plot
    pops = (objs_a, objs_b, objs_c)
    labels = ("A", "B", "C")
    plot(pops, labels)

    # grid properties
    lb = np.array([0, 0])
    ub = np.array([8, 8])
    hs = np.array([1, 1])

    # get locations
    locs_a = get_grid_locs(objs_a, lb, hs)
    locs_b = get_grid_locs(objs_b, lb, hs)
    locs_c = get_grid_locs(objs_c, lb, hs)
    
    # get nds solutions
    merged_pops = np.vstack((objs_a, objs_b, objs_c))
    fronts, ranks = vfns(merged_pops, procs=4)
    best_front = np.array(fronts[0], dtype=int)
    objs_nds = merged_pops[best_front]
    
    # get nds location
    locs_nds = get_grid_locs(objs_nds, lb, hs)
    
    # get nds hyperboxes
    # we use a dictionary for efficient lookup and deletion of duplicate rows in locs_nds
    hyperboxes = {}
    
    for hb_loc in locs_nds:
        
        key = tuple(hb_loc)
        
        if not key in hyperboxes:
            hyperboxes[key] = 1         # hyperboxes[loc] contains the number of solutions in loc
        
        else:
            hyperboxes[key] += 1
            
            
    
    # merge pops
    locs_pops = (locs_a, locs_b, locs_c)
    
    # compute contribution degree
    n_pops = len(locs_pops)
    n_hyperboxes = len(hyperboxes)
    
    cd_matrix = np.zeros((n_hyperboxes, n_pops))
    
    for row, hyperbox in enumerate(hyperboxes):
        
        for col, locs_pop in enumerate(locs_pops):
            
            # compute contribution degree
            cd_matrix[row, col] = get_contribution_degree(locs_pop, hyperbox)
            
            # debug
            print("CD(P[%d], h=%s): %.4f" % (col, hyperbox, cd_matrix[row, col]))
            
        # debug
        print("---")
        
    # compute dci
    by_col = 0
    dci_vals1 = cd_matrix.sum(axis=by_col) / n_hyperboxes
    
    
    # ---- same function ----
    pops = (objs_a, objs_b, objs_c)
    merged_pops = np.vstack(pops)
    z_ideal = np.array([0, 0])
    z_nadir = merged_pops.max(axis=0)
    divs = 8
    dci_vals2 = dci(pops, z_ideal, z_nadir, divs, procs=4, debug=True)
    
    
    
    
