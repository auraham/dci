# test_dci_norm_dtlz2.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import rcParams
from mpl_toolkits.mplot3d import Axes3D
import os, sys
np.seterr(all='raise')

from vfns import vfns
from dci_norm import dci_norm
from plotting import load_rcparams, plot_pops, plot_iso

def load_objs(fev):
    
    moea_name = "amoead_imss_v1-no-form-sdci_estimate_prev-25-mean-10-ps-normal-ss-gra"
    filename = "objs_%s_m_10_run_2_t_550_fev_%s.txt" % (moea_name, fev)
    filepath = os.path.join("sample_pops", "dtlz2", filename)
    
    objs = np.genfromtxt(filepath)
    
    return objs.copy()
    
def vector_str(v):
    
    line = " ".join(["%.10f" % val for val in v])
    return line

if __name__ == "__main__":
    
    # load objs
    objs_a = load_objs("ws")
    objs_b = load_objs("te")
    objs_c = load_objs("asf")
    objs_d = load_objs("pbi")
    objs_e = load_objs("ipbi")
    objs_f = load_objs("vads")
    
    # make a list of pops
    pops = (objs_a, objs_b, objs_c, objs_d, objs_e, objs_f)
    labels = ("ws", "te", "asf", "pbi", "ipbi", "vads")
    
    # get non-dominated solutions using vfns
    merged_pops = np.vstack(pops)
    fronts, ranks = vfns(merged_pops, procs=4)
    best_front = np.array(fronts[0], dtype=int)
    objs_nds = merged_pops[best_front]
    
    # get reference vectors from non-dominated solutions
    z_ideal = objs_nds.min(axis=0)
    z_nadir = objs_nds.max(axis=0)
    
    # print vectors
    print("z_ideal:", vector_str(z_ideal))
    print("z_nadir:", vector_str(z_nadir))
    
    # compute dci
    divs = 19
    dci_vals = dci_norm(pops, divs, procs=4, debug=True)

    # print results
    for i, val in enumerate(dci_vals):
        
        pop_label = labels[i]
        print("DCI(%s) \t= %.4f" % (pop_label, val))

