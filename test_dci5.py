# test_dci5.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import os, sys

np.seterr(all='raise')

# add lab's path to sys.path
lab_path = os.path.abspath("../../")
sys.path.insert(0, lab_path)

from rocket.plot import load_rcparams, plot_pops, colors, basecolors
from rocket.moea.nds import vfns
from dci import get_grid_dims, dci
from sdci import sdci, sdci_debug, sdci_debug2, sdci_debug3
from rocket.moea.nds import vfns


def plot(pops, labels, merged_pops, best_front, save=False):
    
    #load_rcparams((18, 4.2))
    #load_rcparams((22.5, 4.2))
    load_rcparams((27, 4.2))
    
    fig = plt.figure()
    axes = (fig.add_subplot(161, projection="3d"),
            fig.add_subplot(162, projection="3d"),
            fig.add_subplot(163, projection="3d"),
            fig.add_subplot(164, projection="3d"),
            fig.add_subplot(165, projection="3d"),
            fig.add_subplot(166, projection="3d"),
    )
    
    letters = ("(a)", "(b)", "(c)", "(d)", "(e)")
    
    for i, pop in enumerate(pops):
    
        ax = axes[i]
    
        ax.plot(pop[:, 0], pop[:, 1], pop[:, 2], label=labels[i], marker="o", ls="none", c=colors[0], 
            mec=basecolors["almost_black"], markeredgewidth=0.15)
    
    
        ax.set_title("%s %s" % (letters[i], labels[i]))
        #ax.legend(loc="upper right", ncol=1, scatterpoints=1, numpoints=1)

        
        ax.set_xlabel("$f_1$")
        ax.set_ylabel("$f_2$")
        ax.set_zlabel("$f_3$")
        
        ax.set_xlim(0, 1.05)
        ax.set_ylim(0, 1.05)
        ax.set_zlim(0, 1.05)
        
        #ax.set_xticklabels()
        ax.grid()
        ax.view_init(30, 45)
        
        
    # last ax
    ax = axes[-1]
    
    ax.plot(merged_pops[:, 0], merged_pops[:, 1], merged_pops[:, 2], label="merged_pops", marker="o", ls="none", c=colors[0], 
            mec=basecolors["almost_black"], markeredgewidth=0.15)
    
    ax.plot(merged_pops[best_front, 0], merged_pops[best_front, 1], merged_pops[best_front, 2], label="merged_pops[nds]", marker="o", ls="none", c=colors[1], 
            mec=basecolors["almost_black"], markeredgewidth=0.15)
            
    ax.set_xlabel("$f_1$")
    ax.set_ylabel("$f_2$")
    ax.set_zlabel("$f_3$")
    
    ax.set_xlim(0, 2.05)
    ax.set_ylim(0, 2.05)
    ax.set_zlim(0, 2.05)
    
    #ax.set_xticklabels()
    ax.grid()
    ax.view_init(30, 45)
    
    ax.legend(loc="upper right", scatterpoints=1, numpoints=1)
    
    
    # setup main title
    #fig.suptitle(mop_label, 
    #    fontsize=14, 
    #    horizontalalignment="center")
    
    # setup margins
    plt.subplots_adjust(
            bottom=0.12,
            wspace=0.1,
            left=0.04, 
            right=0.98,
            )
    
    if save:
        output = "fig_dci_example_approximations_z_nadir.png"
        fig.savefig(output, dpi=300)
        print(output)
        
    plt.show()

def plot_iso(merged_pops, best_front, save=False):
    
    #load_rcparams((18, 4.2))
    #load_rcparams((22.5, 4.2))
    load_rcparams((8, 8))
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    
    ax.plot(merged_pops[:, 0], merged_pops[:, 1], merged_pops[:, 2], label="merged_pops", marker="o", ls="none", c=colors[0], 
            mec=basecolors["almost_black"], markeredgewidth=0.15)
    
    ax.plot(merged_pops[best_front, 0], merged_pops[best_front, 1], merged_pops[best_front, 2], label="merged_pops[nds]", marker="o", ls="none", c=colors[1], 
            mec=basecolors["almost_black"], markeredgewidth=0.15)
            
    ax.set_xlabel("$f_1$")
    ax.set_ylabel("$f_2$")
    ax.set_zlabel("$f_3$")
    
    ax.set_xlim(0, 2.05)
    ax.set_ylim(0, 2.05)
    ax.set_zlim(0, 2.05)
    
    #ax.set_xticklabels()
    ax.grid()
    ax.view_init(30, 45)
    
    ax.legend(loc="lower right", scatterpoints=1, numpoints=1)
    
    
    # setup main title
    #fig.suptitle(mop_label, 
    #    fontsize=14, 
    #    horizontalalignment="center")
    
    # setup margins
    plt.subplots_adjust(
            bottom=0.12,
            wspace=0.1,
            left=0.04, 
            right=0.98,
            )
    
    if save:
        output = "fig_dci_example_approximations_z_nadir_iso.png"
        fig.savefig(output, dpi=300)
        print(output)
        
    plt.show()

if __name__ == "__main__":
    
    
    # load objs
    objs_a = np.genfromtxt("data/weights_m_3_a_zeros.txt")
    objs_b = np.genfromtxt("data/weights_m_3_b_zeros.txt")
    objs_c = np.genfromtxt("data/weights_m_3_c_zeros.txt")
    objs_d = np.genfromtxt("data/weights_m_3_d_zeros.txt")
    
    objs_e = objs_a*1.5
    
    pops = (objs_a, objs_b, objs_c, objs_d, objs_e)
    labels = ("$P_1$", "$P_2$", "$P_3$", "$P_4$", "$P_5$")
    
    # get nds solutions
    merged_pops = np.vstack(pops)
    fronts, ranks = vfns(merged_pops, procs=4)
    best_front = np.array(fronts[0], dtype=int)
    objs_nds = merged_pops[best_front]
    
    
    # plot
    plot(pops, labels, merged_pops, best_front, save=False)
    plot_iso(merged_pops, best_front, save=False)
    
    # get reference vectors
    merged_pops = np.vstack(pops)
    z_ideal = merged_pops.min(axis=0)
    z_nadir = merged_pops.max(axis=0)
    
    # test different points
    z_nadir_pool = (np.array([1,1,1]), z_nadir.copy(), np.array([3, 3, 3]), np.array([5, 5, 5]))
    
    for z_nadir in z_nadir_pool:
        
        print("z_nadir: %s" % z_nadir)
        
        # get dci
        divs = 19
        dci_vals  = dci(pops, z_ideal, z_nadir, divs, procs=4, debug=False)
        sdci_vals = sdci(pops, divs, procs=4, debug=False)
        sdci_vals_debug = sdci_debug(pops, z_ideal, z_nadir, divs, procs=4, debug=False)
        sdci_vals_debug2 = sdci_debug2(pops, z_ideal, z_nadir, divs, procs=4, debug=False)
        sdci_vals_debug3 = sdci_debug3(pops, divs, procs=4, debug=False)
        
        # grid properties
        lb, ub, hs = get_grid_dims(z_ideal, z_nadir, divs)
        print("lb: %s, ub: %s, hs: %s" % (lb, ub, hs))

        # print results
        lbls = ("P1", "P2", "P3", "P4", "P5")
        for i in range(len(pops)):
            print("DCI(%s) : %.4f  SDCI(%s): %.4f  SDCI D1 (%s): %.4f, SDCI D2 (%s): %.4f, SDCI D3 (%s): %.4f" % (lbls[i], dci_vals[i], lbls[i], sdci_vals[i], lbls[i], sdci_vals_debug[i], lbls[i], sdci_vals_debug2[i], lbls[i], sdci_vals_debug3[i]))

        print("")
    
    
    
    
