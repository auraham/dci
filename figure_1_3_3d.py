# figure_1_3_3d.py
# %run figure_1_3_3d.py
from __future__ import print_function
import numpy as np
from numpy.linalg import norm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from pylab import rcParams
import os, sys, argparse
np.seterr(all='raise')

# add path to rocket
sys.path.insert(0, "/home/auraham/moead_mss_dci")
from rocket.pareto_fronts import get_front
from rocket.fronts import get_real_front
from rocket.plot.colors import colors, basecolors

def load_objs(mop_name, m_objs, t=500, run_id=0, experiment_path="results", suffix=""):
    
    moea_name = "amoead-pbi-5"
    filename = "objs_%s_m_%d_run_%d_t_%d%s.txt" % (moea_name, m_objs, run_id, t, suffix)
    filepath = os.path.join(experiment_path, mop_name, moea_name, "pops", filename)
    
    objs = np.genfromtxt(filepath)
    return objs.copy()

def load_rcparams():
    """
    Load a custom rcParams dictionary
    """
    
    # setup figure size
    width = 6.465
    height = 2.2
    height = 2
    figsize = (width, height)
    rcParams['figure.figsize'] = figsize
    
    # style
    plt.rc('font', family='serif', serif='Times')
    plt.rc('text', usetex=True)
    plt.rc('xtick', labelsize=5)
    plt.rc('ytick', labelsize=5)
    plt.rc('axes', labelsize=8, titlesize=9)
    plt.rc('legend', fontsize=9)
    
if __name__ == "__main__":
    
    args = {
        "mop_name": "dtlz1",
        "m_objs": 3,
    }
    
    mop_lbls = {"maf1": "MaF1",
                "dtlz1": "DTLZ1",
                "maf4": "MaF4",
                "inv-dtlz1": "inv-DTLZ1"}
    
    load_rcparams()
    save = True
    
    # pareto front
    front = get_front(args["mop_name"], args["m_objs"])
    
    real_front, p = get_real_front(args["mop_name"], args["m_objs"])
    real_front *= 2
    
    
    fig = plt.figure()
    axes = (fig.add_subplot(131, projection="3d"),
            fig.add_subplot(132, projection="3d"),
            fig.add_subplot(133, projection="3d"),
            )
    first_ax = axes[0]
            
    t_iter = 500
    letters = ("a", "b", "c")
    titles = ("Population before adaptation", "Population before filtering", "Population after filtering")
    
    # ----
    from rocket.moea.nds import vfns
    from dci import get_grid_dims, dci
    
    # load objs
    objs_a = np.genfromtxt("data/weights_m_3_a_zeros.txt")
    objs_b = np.genfromtxt("data/weights_m_3_b_zeros.txt")
    objs_c = np.genfromtxt("data/weights_m_3_c_zeros.txt")
    objs_d = np.genfromtxt("data/weights_m_3_d_zeros.txt")
    
    pops = (objs_a, objs_b,  objs_c)# , objs_d)
    labels = ("$P_1$", "$P_2$", "$P_3$")#, "$P_4$")
    
    # get reference vectors
    merged_pops = np.vstack(pops)
    z_ideal = merged_pops.min(axis=0)
    z_nadir = merged_pops.max(axis=0)
    
    # get dci
    divs = 19
    dci_vals = dci(pops, z_ideal, z_nadir, divs, procs=4, debug=False)
    
    # grid properties
    lb, ub, hs = get_grid_dims(z_ideal, z_nadir, divs)
    
    # plot
    #plot(pops, labels, save=True)

    # print results
    lbls = ("P1", "P2", "P3", "P4")
    for i in range(len(pops)):
        print("DCI(%s): %.4f" % (lbls[i], dci_vals[i]))
        
    # new labels
    
    labels = [ "DCI($P_%d$) = %.4f" % (i+1, dci_vals[i]) for i in range(len(pops)) ]
    
    # ----
    
    # setup axes:
    for ax_id, ax in enumerate(axes):
        
        ax.cla()
        
        pop = pops[ax_id]
        
        ax.xaxis.set_rotate_label(False)
        ax.yaxis.set_rotate_label(False)
        ax.zaxis.set_rotate_label(False)
        
        ax.set_xlabel("$f_1$", labelpad=-5, rotation=0)
        ax.set_ylabel("$f_2$", labelpad=-5, rotation=0)
        ax.set_zlabel("$f_3$", labelpad=-8.4, rotation=0)
        
        ax.w_xaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))
        ax.w_yaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))
        ax.w_zaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))
        
        if args["mop_name"] == "inv-dtlz1":
            ax.set_xlim((-0.05, 0.55))
            ax.set_ylim((-0.05, 0.55))
            ax.set_zlim((-0.05, 0.55))
            
        if args["mop_name"] == "maf1":
            ax.set_xlim((-0.05, 1.08))
            ax.set_ylim((-0.05, 1.05))
            ax.set_zlim((-0.05, 1.05))
        
        if args["mop_name"] == "dtlz1":
            ax.set_xlim((-0.05, 1.08))
            ax.set_ylim((-0.05, 1.05))
            ax.set_zlim((-0.05, 1.05))
            
        if args["mop_name"] == "maf4":
            ax.set_xlim((-0.05, 2.14))
            ax.set_ylim((-0.05, 4.25))
            ax.set_zlim((-0.05, 8.12))
            
            ticks = [0, 2, 4, 6, 8]
            ticks = [0, 1, 2];      ax.set_xticks(ticks); ax.set_xticklabels(["%d" % v for v in ticks])
            ticks = [0, 2, 4];      ax.set_yticks(ticks); ax.set_yticklabels(["%d" % v for v in ticks])
            ticks = [0, 2, 4, 6, 8];ax.set_zticks(ticks); ax.set_zticklabels(["%d" % v for v in ticks])
            
        # adjust length
        ax.xaxis.set_tick_params(length=2)
        ax.yaxis.set_tick_params(length=2)
        
        # space between labels and axes
        ax.tick_params(axis="x", which="major", pad=-3)
        ax.tick_params(axis="y", which="major", pad=-3)
        ax.tick_params(axis="z", which="major", pad=-3)
        
        # ---
        
        ax.xaxis._axinfo["grid"].update({"linewith": 0.2, "color": "#e6e6e6"})
        ax.yaxis._axinfo["grid"].update({"linewith": 0.2, "color": "#e6e6e6"})
        ax.zaxis._axinfo["grid"].update({"linewith": 0.2, "color": "#e6e6e6"})
        
        # ---
        
        # plot meshgrid
        X = real_front[:, 0].reshape(p, p)
        Y = real_front[:, 1].reshape(p, p)
        Z = real_front[:, 2].reshape(p, p)
        ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2, color=basecolors["real_front"], lw=0.2)
        
        # plot pop
        ax.plot(pop[:, 0], pop[:, 1], pop[:, 2], label=labels[i], marker="o", ls="none", c=colors[0], 
            mec=basecolors["almost_black"], markeredgewidth=0.15, ms=2)
    
        
        # plot pareto front
        #ax.plot(front[:, 0], front[:, 1], front[:, 2], c="#DEDEDE", label="Pareto front",
        #                #mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15, ms=2)    
        #                mec="#828282", ls="none", marker="o", markeredgewidth=0.15, ms=2)    
            
        title = "(%s) %s, $t=%d$\n%s" % (letters[ax_id], mop_lbls[args["mop_name"]], t_iter, titles[ax_id])
        title = "(%s) %s" % (letters[ax_id], labels[ax_id])
        ax.set_title(title, y=-0.46)
        ax.view_init(30, 45)
    
    """
    # setup legend (smaller height)
    leg = first_ax.legend(loc='upper center', 
            bbox_to_anchor=(1.85, 1.25),
            fancybox=False, shadow=False, 
            frameon=False, ncol=2,
            handletextpad=0.5,          # espacio entre "o" y "parents"
            markerscale=1,
            scatterpoints=1, numpoints=1,
            handlelength=1)
    """
    
    """        
    leg_lines = leg.get_lines()
    plt.setp(leg_lines, linewidth=1)
    """

    # adjust margins
    fig.subplots_adjust(left=0.07,
                        bottom=0.26,
                        right=0.96,
                        top=0.88,
                        wspace=0.4,
                        hspace=0.5,
                        )

    # save figure
    if save:
        for fmt in ("eps", ):
            output = "figure_1_3_3d.%s" % fmt
            #fig.savefig(output, dpi=300, facecolor="#f0f0f0")   # debug
            fig.savefig(output, dpi=300)
            print(output)


