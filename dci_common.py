# dci_common.py
from __future__ import print_function
import numpy as np
from scipy.spatial.distance import cdist

def vector_str(v):
    return " ".join(["%.8f" % val for val in v ])


def normalize(objs, z_ideal, z_nadir):
    """
    Normalization of objective vectors, as recommended by 
    [He18] Evolutionary Many-objective Optimization based on Dynamical Decomposition
    
    Input
    objs        (pop_size, m_objs) objs matrix
    z_ideal     (m_objs, ) ideal vector
    z_nadir     (m_objs, ) nadir vector
    
    Output
    norm_objs   (pop_size, m_objs) norm objs matrix
    """
    
    pop_size, m_objs = objs.shape

    # alias
    FX = objs
    Z_ideal = z_ideal.reshape((1, m_objs))
    Z_nadir = z_nadir.reshape((1, m_objs))
    
    # divisor
    div = Z_nadir - Z_ideal
    
    if np.any(div == 0):
        eps = np.finfo(float).eps                   # https://docs.scipy.org/doc/numpy/reference/generated/numpy.finfo.html
        mask = div == 0
        div[mask] = eps
    
    norm_objs = (FX - Z_ideal) / div
    
    return norm_objs.copy()


def create_groups(best_front, pop_sizes):
    """
    Return a tuple of n_pops tuples, called groups, where groups[i] contains
    the indices of those solutions in best_front from the i-th population.
    """
    
    n_pops = len(pop_sizes)
    groups = [[] for i in range(n_pops)]
    
    for ind in best_front:
        
        start = 0
        end = 0
        
        for pop_ind in range(n_pops):
            
            if pop_ind == 0:
                start = 0
            else:
                start = end
                
            end = start + pop_sizes[pop_ind]
            
            # if ind is in range [start, end], 
            # then add it to the group
            if ind >= start and ind < end:
                groups[pop_ind].append(ind)
                break
                
    return tuple([ tuple(group) for group in groups ])


def get_grid_locs(objs, lb, hs):
    """
    Return a (pop_size, m_objs) matrix with the location of each objective vector
    within the grid according to Eq. (5) in [Li14].
    
    Note: be sure to call either filter_pop() or filter_pops() before calling this function.
    
    Input
    objs        (pop_size, m_objs) obj matrix
    lb          (m_objs, ) lower boundaries of the grid, where lb[k] is the lower boundary of the k-th dimension (objective)
    hs          (m_objs, ) hyperbox size, where hs[k] is the hyperbox size in the k-th dimension (objective)
    
    Output
    locs        (pop_size, m_objs) location matrix
    """
    
    pop_size, m_objs = objs.shape
    
    # reshape
    lb_row = lb.copy().reshape((1, m_objs))
    hs_row = hs.copy().reshape((1, m_objs))
    
    # ADDED
    # to avoid division by zero
    if np.any(hs_row == 0):
        eps = np.finfo(float).eps                   # https://docs.scipy.org/doc/numpy/reference/generated/numpy.finfo.html
        mask = hs_row == 0
        hs_row[mask] = eps
    
    locs = np.floor((objs - lb_row) / hs_row)       # (pop_size, m_objs), Eq (5)

    locs = np.array(locs, dtype=int)                # cast to int
    
    return locs.copy()


def get_contribution_degree(locs_pop, hyperbox):
    """
    
    Input
    locs_pop        (pop_size, m_objs) int matrix, location of the pop in the grid
    hyperbox        (m_objs, ) tuple, location of the hyperbox in the grid
    
    """
    
    # contribution degree
    cd = 0 
    
    pop_size, m_objs = locs_pop.shape
    
    # cast hyperbo
    h_row = np.array(hyperbox, dtype=float).reshape((1, m_objs))
    
    # cast pop
    pop = np.array(locs_pop, dtype=float)
    
    # compute distance between pop and hyperbox
    dist = cdist(h_row, pop, "euclidean")           # (1, pop_size)     Eq (6) in [Li14]
    
    min_dist = dist.min()                           # D(P, h), Eq (7) in [Li14]
    
    # compute contribution degree
    thresh = np.sqrt(m_objs + 1)
    
    if min_dist < thresh:
        
        cd = 1 - ((min_dist**2) / (m_objs+1)) 
    else:
        
        cd = 0
        
    return cd
        

def get_grid_dims(z_ideal, z_nadir, divs):
    """
    Return the size of hyperbox and the boundaries of the grid
    
    Input
    z_ideal     (m_objs, ) array, ideal vector
    z_nadir     (m_objs, ) array, nadir vector
    divs        int, number of divisions of the grid
    
    Output
    lb          (m_objs, ) array, lower boundaries of the grid
    ub          (m_objs, ) array, upper boundaries of the grid
    hs          float, size of hyperbox
    """
    
    lb = z_ideal.copy()
    
    # -- added to avoid FloatingPointError
    ub = None
    try:
        ub = z_nadir + ((z_nadir - z_ideal)/(2*divs))
    
    except FloatingPointError:
        
        print("Handling FloatingPointError in sdci.py")
        
        num = z_nadir - z_ideal
        
        eps  = np.finfo(float).eps                  # https://docs.scipy.org/doc/numpy/reference/generated/numpy.finfo.html
        mask = num <= eps
        
        num[mask] = 0                               # truncate tiny values to zero
        
        # compute upper bound here
        ub = z_nadir + (num/(2*divs))
        
        # compute hs here to avoid another FloatingPointError exception
        num = ub - lb
        mask = num <= eps
        num[mask] = 0
        hs = num / divs
        
        return lb.copy(), ub.copy(), hs
        
    
    hs = (ub - lb) / divs
    
    return lb.copy(), ub.copy(), hs 
    

def filter_pop(pop, lb, ub, debug=False):
    """
    Remove solutions in pop out of the grid defined by lb and ub.
    
    Input
    pop         (pop_size, m_objs) objs matrix
    lb          (m_objs, ) array, lower boundaries of the grid
    ub          (m_objs, ) array, upper boundaries of the grid
    
    Output
    new_pop     (pop_size, m_objs) filtered matrix
    """

    # reshape boundaries
    m_objs = lb.shape[0]
    lb_row = lb.reshape((1, m_objs))
    ub_row = ub.reshape((1, m_objs))

    # identify solution within the grid
    to_keep = (pop >= lb_row ) & (pop <= ub_row)
    
    # reshape (pop_size, m_objs) -> (pop_size, )
    to_keep = np.all(to_keep, axis=1)
    
    # filter
    new_pop = pop[to_keep, :].copy()
    
    # debug
    if debug:
        print("This pop has %d in the grid (original size: %d)" % (new_pop.shape[0], pop.shape[0]))
    
    return new_pop


def filter_pops(pops, lb, ub, debug=False):
    """
    Remove solutions in pops out of the grid defined by lb and ub.
    
    Input
    pops        list of (pop_size, m_objs) objs matrices
    lb          (m_objs, ) array, lower boundaries of the grid
    ub          (m_objs, ) array, upper boundaries of the grid
    
    Output
    new_pops    list of (pop_size, m_objs) filtered matrices
    """
    
    new_pops = []
    
    m_objs = lb.shape[0]
    lb_row = lb.reshape((1, m_objs))
    ub_row = ub.reshape((1, m_objs))
    
    for i, pop in enumerate(pops):

        # identify solution within the grid
        to_keep = (pop >= lb_row ) & (pop <= ub_row)
        
        # reshape (pop_size, m_objs) -> (pop_size, )
        to_keep = np.all(to_keep, axis=1)
        
        # keep those solutions
        new_pop = pop[to_keep].copy()
        
        # add new pop to the list
        new_pops.append(new_pop.copy())
        
        # debug
        if debug:
            print("pop %d has %d in the grid (original size: %d)" % (i, new_pop.shape[0], pop.shape[0]))
        
    return new_pops


def normalize_pops(pops, z_ideal, z_nadir):
    """
    Normalize populations
    
    Input
    pops        list of (pop_size, m_objs) objs matrices
    z_ideal     (m_objs, ) ideal vector
    z_nadir     (m_objs, ) nadir vector
    
    Output
    norm_pops   list of (pop_size, m_objs) normalized matrices
    """
    
    norm_pops = []
    
    for pop in pops:
        
        norm_pop = normalize(pop, z_ideal, z_nadir)
        
        norm_pops.append(norm_pop.copy())
        
    return norm_pops
    
