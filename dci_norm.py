# dci_norm.py
from __future__ import print_function
import numpy as np
from scipy.spatial.distance import cdist
from vfns import vfns
from dci_common import get_grid_locs, get_grid_dims
from dci_common import get_contribution_degree
from dci_common import normalize, normalize_pops
from dci_common import filter_pop, filter_pops
from dci_common import vector_str

def dci_norm(pops, divs, procs=4, debug=False):
    """
    Compute the DCI indicator using normalization.
    
    Input
    pops        list of (pop_size, m_objs) objs matrices
    divs        int, number of divisions of the grid
    procs       int, number of processes for nds
    
    Output
    dci_vals    (n_pops, ) dci values
    """
    
    # 1. get non-dominated solutions
    merged_pops = np.vstack(pops)
    fronts, ranks = vfns(merged_pops, procs)
    best_front = np.array(fronts[0], dtype=int)
    objs_nds = merged_pops[best_front]
    
    # debug
    if debug:
        print("The number of non-dominated solutions is %d" % objs_nds.shape[0]);
    
    # 2. obtain z_ideal, z_nadir from objs_nds
    z_ideal = objs_nds.min(axis=0)
    z_nadir = objs_nds.max(axis=0)
    
    # 3. normalize pops and objs_nds
    norm_pops = normalize_pops(pops, z_ideal, z_nadir)
    norm_objs_nds = normalize(objs_nds, z_ideal, z_nadir)
    
    # 4. compute grid dimesions
    m_objs = norm_objs_nds.shape[1]
    ref_ideal = np.zeros((m_objs, ))
    ref_nadir = np.ones((m_objs, ))
    lb, ub, hs = get_grid_dims(ref_ideal, ref_nadir, divs)
    
    # debug
    if debug:
        print("lb: %s" % vector_str(lb))
        print("ub: %s" % vector_str(ub))
        print("hs: %s" % vector_str(hs))
    
    # remove solutions out of the grid
    pops = filter_pops(norm_pops, lb, ub, debug)
    objs_nds = filter_pop(norm_objs_nds, lb, ub, debug)
    
    # get locations of pops in grid
    locs_pops = []
    
    for objs_pop in pops:
        
        # (pop_size, m_objs) int matrix, it contains the location of
        # the obj vectors in objs_pop within the grid
        locs_pop = get_grid_locs(objs_pop, lb, hs)
        
        # add it to the list
        locs_pops.append(locs_pop.copy())
    
    # get nds location
    locs_nds = get_grid_locs(objs_nds, lb, hs)
    
    # get nds hyperboxes
    # we use a dictionary for efficient lookup and deletion of duplicate rows in locs_nds
    hyperboxes = {}
    
    for hb_loc in locs_nds:
        
        # cast from np.array to tuple
        key = tuple(hb_loc)
        
        if not key in hyperboxes:
            
            # hyperboxes[loc] contains the number of solutions in the
            # hyperbox defined by loc (although this counter is not necessary for computing DCI)
            hyperboxes[key] = 1         
        
        else:
            
            # increment counter
            hyperboxes[key] += 1
            
            
    
    # compute contribution degree
    n_pops = len(locs_pops)
    n_hyperboxes = len(hyperboxes)
    
    # debug
    if debug:
        print("The number of boxes where all solutions locate is %d" % n_hyperboxes);
    
    cd_matrix = np.zeros((n_hyperboxes, n_pops))
    
    for row, hyperbox in enumerate(hyperboxes):
        
        for col, locs_pop in enumerate(locs_pops):
            
            # check size
            if locs_pop.shape[0] > 0:
            
                # compute contribution degree
                cd_matrix[row, col] = get_contribution_degree(locs_pop, hyperbox)
            
        
    # compute dci
    by_col = 0
    dci_vals = cd_matrix.sum(axis=by_col) / n_hyperboxes
    
    return dci_vals.copy()
