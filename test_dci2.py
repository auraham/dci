# test_dci2.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import os, sys

np.seterr(all='raise')

# add lab's path to sys.path
lab_path = os.path.abspath("../../")
sys.path.insert(0, lab_path)

from rocket.plot import load_rcparams, plot_pops, colors, basecolors
from rocket.moea.nds import vfns
from dci import get_grid_dims, dci

def plot(pops, labels, hs, save=False):
    
    load_rcparams((8, 8))
    
    ticks = [i*hs for i in range(9)]
    ticklabels = ["%.3f" % i for i in ticks]
    
    
    #fig, ax = plot_pops(pops, labels, ncol=3)
    
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    markers = ("s", "o", "o")
    ccolors = ("#ffffff", "#000000", "#ffffff")
    
    for i, pop in enumerate(pops):
    
        ax.plot(pop[:, 0], pop[:, 1], label=labels[i], marker=markers[i], ls="none", c=ccolors[i], 
            mec="#000000", markeredgewidth=1)
    
    ax.legend(loc="upper right", ncol=1, scatterpoints=1, numpoints=1)
    
    ax.set_xlim(0, 8)
    ax.set_ylim(0, 8)
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.set_xticklabels(ticklabels)
    ax.set_yticklabels(ticklabels)
    ax.set_xlabel("$f_1$")
    ax.set_ylabel("$f_2$")
    #ax.set_xticklabels()
    ax.grid()
    
    if save:
        output = "fig_dci_example.png"
        fig.savefig(output, dpi=300)
        print(output)
    
    plt.show()
    

def load_objs():
    
    objs_a = np.array([
                    [0.4, 6.8],
                    [1.5, 4.5],
                    [2.5, 2.8],
                    [4.2, 2.2],
                    [5.8, 1.2],
                    [7.5, 0.2],
                    ])
                    
    objs_b = np.array([
                    [0.2, 7.52],
                    [0.8, 6.2],
                    [4.9, 1.8],
                    [6.4, 0.6],
                    [7.2, 0.4],
                    [4.4, 5.8],
                    ])
                    
    objs_c = np.array([
                    [1.2, 4.8],
                    [1.9, 3.5],
                    [3.2, 2.6],
                    [3.9, 2.5],
                    [4.6, 2.2],
                    [7.52, 2.3],
                    ])
                    
    return objs_a.copy(), objs_b.copy(), objs_c.copy()


if __name__ == "__main__":
    
    
    # load objs
    objs_a, objs_b, objs_c = load_objs()
    
    pops = (objs_a, objs_b, objs_c)
    labels = ("$P_1$", "$P_2$", "$P_3$")
    
    # get reference vectors
    merged_pops = np.vstack(pops)
    z_ideal = np.array([0, 0])
    z_nadir = merged_pops.max(axis=0)
    
    # get dci
    divs = 8
    dci_vals = dci(pops, z_ideal, z_nadir, divs, procs=4, debug=True)
    
    # grid properties
    lb, ub, hs = get_grid_dims(z_ideal, z_nadir, divs)
    
    # plot
    plot(pops, labels, hs[0], save=False)

    
    
    
    
    
