# test_dci3.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.pyplot import rcParams

import os, sys

np.seterr(all='raise')

# add lab's path to sys.path
lab_path = os.path.abspath("../../")
sys.path.insert(0, lab_path)

from rocket.plot import load_rcparams, plot_pops, colors, basecolors
from rocket.moea.nds import vfns
from dci import get_grid_dims, dci

def plot(pops, labels, save=False):
    
    load_rcparams((18, 4.2))
    
    rcParams['axes.titlesize']  = 20            # title
    rcParams['axes.labelsize']  = 20            # $f_i$ labels
    #rcParams['xtick.color']     = "#474747"     # ticks gray color
    #rcParams['ytick.color']     = "#474747"     # ticks gray color
    #rcParams['xtick.labelsize'] = 10            # ticks size
    #rcParams['ytick.labelsize'] = 10            # ticks size
    #rcParams['legend.fontsize'] = 12            # legend
    #rcParams['legend.fontsize'] = 12            # legend

    
    
    fig = plt.figure()
    axes = (fig.add_subplot(141, projection="3d"),
            fig.add_subplot(142, projection="3d"),
            fig.add_subplot(143, projection="3d"),
            fig.add_subplot(144, projection="3d"),
    )
    
    letters = ("(a)", "(b)", "(c)", "(d)")
    
    for i, pop in enumerate(pops):
    
        ax = axes[i]
    
        ax.plot(pop[:, 0], pop[:, 1], pop[:, 2], label=labels[i], marker="o", ls="none", c=colors[0], 
            mec=basecolors["almost_black"], markeredgewidth=0.15)
    
    
        ax.set_title("%s %s" % (letters[i], labels[i]))
        #ax.legend(loc="upper right", ncol=1, scatterpoints=1, numpoints=1)

        
        ax.set_xlabel("$f_1$")
        ax.set_ylabel("$f_2$")
        ax.set_zlabel("$f_3$")
        
        ax.set_xlim(0, 1.05)
        ax.set_ylim(0, 1.05)
        ax.set_zlim(0, 1.05)
        
        #ax.set_xticklabels()
        ax.grid()
        ax.view_init(30, 45)
    
    # setup main title
    #fig.suptitle(mop_label, 
    #    fontsize=14, 
    #    horizontalalignment="center")
    
    # setup margins
    plt.subplots_adjust(
            bottom=0.12,
            wspace=0.1,
            left=0.04, 
            right=0.98,
            )
    
    if save:
        output = "fig_dci_example_approximations.eps"
        fig.savefig(output, dpi=300)
        print(output)
        
    plt.show()

if __name__ == "__main__":
    
    
    # load objs
    objs_a = np.genfromtxt("data/weights_m_3_a_zeros.txt")
    objs_b = np.genfromtxt("data/weights_m_3_b_zeros.txt")
    objs_c = np.genfromtxt("data/weights_m_3_c_zeros.txt")
    objs_d = np.genfromtxt("data/weights_m_3_d_zeros.txt")
    
    pops = (objs_a, objs_b, objs_c, objs_d)
    labels = ("$P_1$", "$P_2$", "$P_3$", "$P_4$")
    
    # get reference vectors
    merged_pops = np.vstack(pops)
    z_ideal = merged_pops.min(axis=0)
    z_nadir = merged_pops.max(axis=0)
    
    # get dci
    divs = 19
    dci_vals = dci(pops, z_ideal, z_nadir, divs, procs=4, debug=False)
    
    # grid properties
    lb, ub, hs = get_grid_dims(z_ideal, z_nadir, divs)
    
    # plot
    plot(pops, labels, save=True)

    # print results
    lbls = ("P1", "P2", "P3", "P4")
    for i in range(len(pops)):
        print("DCI(%s): %.4f" % (lbls[i], dci_vals[i]))
    
    
    
    
    
