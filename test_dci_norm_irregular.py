# test_dci_norm_irregular.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import rcParams
from mpl_toolkits.mplot3d import Axes3D
import os, sys
np.seterr(all='raise')

from vfns import vfns
from dci_norm import dci_norm
from plotting import load_rcparams, plot_irregular_pops

if __name__ == "__main__":
    
    
    # load objs
    objs_a = np.genfromtxt("data/weights_m_3_a_zeros.txt")
    objs_b = np.genfromtxt("data/weights_m_3_b_zeros.txt")
    objs_c = np.genfromtxt("data/weights_m_3_c_zeros.txt")
    objs_d = np.genfromtxt("data/weights_m_3_d_zeros.txt")
    objs_e = objs_a*1.5
    
    # make a list of pops
    pops = (objs_a, objs_b, objs_c, objs_d, objs_e)
    labels = ("$P_1$", "$P_2$", "$P_3$", "$P_4$", "$P_5$")

    # adjust range
    m_objs = 3
    scales = [1, 10, 100]
    new_pops = []
    
    for pop in pops:
    
        new_pop = pop.copy()
    
        for m in range(m_objs):
            new_pop[:, m] = pop[:, m] * scales[m]
            
        new_pops.append(new_pop.copy())
        
    # rename
    pops = tuple([new_pop for new_pop in new_pops])
    
    # plot pops
    plot_irregular_pops(pops, labels, scales, save=True)
    
    # compute dci
    divs = 19
    dci_vals = dci_norm(pops, divs, procs=4, debug=False)

    # print results
    for i, val in enumerate(dci_vals):
        
        pop_label = labels[i].replace("$", "")
        print("DCI(%s) = %.4f" % (pop_label, val))
